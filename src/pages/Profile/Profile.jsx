import React, { useEffect } from "react";
import { Navigate, redirect, useNavigate } from "react-router-dom";

export default function Profile() {
  let navigate = useNavigate();

  useEffect(() => {
    if (localStorage.getItem("userLogin")) {
      return <div>Profile</div>;
    } else {
      alert("Vui lòng đăng nhập để vào trang này!");
      // redirect("/login");
      navigate("/login");
    }
  }, []);
}
