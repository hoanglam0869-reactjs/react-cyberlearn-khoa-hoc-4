import React from "react";
import { matchPath, useLocation, useMatch, useParams } from "react-router-dom";

export default function Detail() {
  let params = useParams();

  let location = useLocation();
  const match = matchPath(
    {
      path: "/detail/:id",
      caseSensitive: false,
      end: true,
    },
    "/detail/1234"
  );
  let m = useMatch("/detail/1234");
  console.log(match);
  console.log(params);
  console.log(location);
  return (
    <div>
      Giá trị tham số: {params.id}
      <br />
      Path name hiện tại: {location.pathname}
    </div>
  );
}
