import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

export default function Login(props) {
  let navigate = useNavigate();

  const [userLogin, setUserLogin] = useState({
    username: "",
    password: "",
    isValid: false,
  });

  useEffect(() => {
    console.log(userLogin);
    return () => {
      alert("Bạn có chắc chắn muốn rời khỏi trang này?");
    };
  }, []);

  const handleChange = (event) => {
    const { name, value } = event.target;

    const newUserLogin = {
      ...userLogin,
      [name]: value,
    };

    let isValid = true;
    for (let key in newUserLogin) {
      if (key != "isValid") {
        if (newUserLogin[key].trim() == "") {
          isValid = false;
        }
      }
    }
    setUserLogin({ ...newUserLogin, isValid: isValid });
  };

  const handleLogin = (event) => {
    event.preventDefault();
    if (
      userLogin.username == "cyberlearn" &&
      userLogin.password == "cyberlearn"
    ) {
      // Thành công thì chuyển về trang trước đó
      // props.history.goBack();

      // Chuyển đến trang chỉ định sau khi xử lý
      // Chuyển hướng đến path tương ứng
      // navigate("/home");

      // replace thay đổi nội dung path tương ứng
      // navigate("/home", { replace: true });
      localStorage.setItem("userLogin", JSON.stringify(userLogin));
      navigate(-1);
    } else {
      alert("Fail");
      return;
    }
  };

  return (
    <form className="container" onSubmit={handleLogin}>
      <h3 className="display-4">Login</h3>
      <div className="form-group">
        <p>Username</p>
        <input
          type="text"
          name="username"
          className="form-control"
          onChange={handleChange}
        />
      </div>
      <div className="form-group mt-4">
        <p>Password</p>
        <input
          type="password"
          name="password"
          className="form-control"
          onChange={handleChange}
        />
      </div>
      <div className="form-group mt-4">
        <button className="btn btn-success">Đăng nhập</button>
      </div>
    </form>
  );
}
