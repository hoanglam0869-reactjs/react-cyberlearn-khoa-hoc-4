import React from "react";
import { NavLink } from "react-router-dom";
import "./Header.css";

export default function Header() {
  const createClass = ({ isActive, isPending }) => {
    return `nav-link${isActive ? " activeNavItem" : ""}`;
  };
  const createStyle = ({ isActive, isPending }) => {
    return isActive ? { fontWeight: "bold" } : {};
  };

  return (
    <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
      <NavLink className="navbar-brand" to="/">
        Cyberlearn
      </NavLink>
      <button
        className="navbar-toggler d-lg-none"
        type="button"
        data-bs-toggle="collapse"
        data-bs-target="#collapsibleNavId"
        aria-controls="collapsibleNavId"
        aria-expanded="false"
        aria-label="Toggle navigation"
      />
      <div className="collapse navbar-collapse" id="collapsibleNavId">
        <ul className="navbar-nav me-auto mt-2 mt-lg-0">
          <li className="nav-item">
            <NavLink
              className={createClass}
              style={createStyle}
              to="/home"
              aria-current="page"
            >
              Home <span className="visually-hidden">(current)</span>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink className={createClass} style={createStyle} to="/contact">
              Contact
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink className={createClass} style={createStyle} to="/about">
              About
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink className={createClass} style={createStyle} to="/login">
              Login
            </NavLink>
          </li>
          <li className="nav-item dropdown">
            <a
              className="nav-link dropdown-toggle"
              href="#"
              id="dropdownId"
              data-bs-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              Bài Tập
            </a>
            <div className="dropdown-menu" aria-labelledby="dropdownId">
              <NavLink className="dropdown-item" to="/todolistrfc">
                To Do List RFC
              </NavLink>
              <NavLink className="dropdown-item" to="/todolistrcc">
                To Do List RCC
              </NavLink>
            </div>
          </li>
        </ul>
        <form className="d-flex my-2 my-lg-0">
          <input
            className="form-control me-sm-2"
            type="text"
            placeholder="Search"
          />
          <button
            className="btn btn-outline-success my-2 my-sm-0"
            type="submit"
          >
            Search
          </button>
        </form>
      </div>
    </nav>
  );
}
